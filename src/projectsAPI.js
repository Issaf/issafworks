let projects = [
  {
    id: "socialis",
    name: "Socialis",
    category: "Productivity",
    url: "https://socialis.io",
    repoUrl: "https://gitlab.com/Issaf/socialis",
    imgUrl: "socialis-bg.jpg",
    description: "Socialis (social in latin) is a small and simple progressive web app that makes it easy to keep track of who you want to contact and when. When creating a new contact, you will have the option to choose on which occasions you want to contact that person. Once you have added all the contacts you want, the app will automatically generate a 'to be contacted' checklist. This is particularly useful for people who are not in your immediate circle (old boss, business contact, accountant, or distant relative for example). The app was designed in one day and mainly for mobile. It is therefore only a first version of th project. Refer to the improvements tab to see the roadmap.",
    problem: "From university and high school, previous jobs, or networking events, we meet a lot of people throughout life. And yet it is quite difficult to keep in touch with everyone. Although sending a short message or calling only takes a couple of minutes, the effort to keep track of who to contact and when can be tedious. That is precisely what socialis is trying to solve.",
    technologies: ["Javascript", "Vue.js", "Vuex", "Vuetify.js", "Progressive Web App", "Firebase Auth", "Firestore", "moment.js"],
    improvements: [
      "Complete a real landing page with nice illustrations, screenshots, customer testimonials, and an explanation of the app", 
      "Add section saying the app can be downloaded and show how", 
      "Improve UI for desktop", 
      "Implement unit and e2e tests", 
      "Add push notifications", 
      "Add more specific triggers for Google Analytics", 
      "Do a second round of performance optimization", 
      "Add feature to import contacts for phone contacts, Facebook, Whatsapp",
      "Add option to create custom occasions", 
      "Improve database security and access permissions", 
      "create a free plan with limited number of contacts, no custom occasions, and no bulk import", 
      "Add stripe and a paid version", 
      "create an automated communication flow (Ex. send pre-defined and dynamic messages to each person to be contacted for new year)", 
      "Add animations and loading placeholders", 
      "Review design and feel of the app"
    ] 
  },
  {
    id: "antibroke",
    name: "Antibroke",
    category: "Finance",
    url: "https://antibroke.firebaseapp.com",
    repoUrl: "https://gitlab.com/Issaf/antibroke",
    imgUrl: "antibroke-bg.jpg",
    description: "Antibroke (lame name in hindsight) is a small progressive web app containg 3 simplified financial calculators. The first one is a gross income calculator given certain conditions. For example what the gross income I need to pay for my living expenses of 1500$/month, save 5000$/year, and donate 5% per month? The second calculator is a simple reverse calculator answering the question ‘How much capital do I need to invest with an ROI of 10% to receive 5000$/month in cashflow? The last one is regular cashflow calculator that takes in the amount of capital to invest, the ROI percentage, and period of investment.",
    problem: "There are many personal income tax calculators but none that do the reverse operation. In fact, you hear the phrases ‘I want to be rich’ or ‘I want to make more money’ a lot but often it is not based on any real number. Antibroke gross income calculator allows people to find out how much they really need to make to have they lifestyle they want.",
    technologies: ["Javascript", "Vue.js", "Vuetify.js", "Progressive Web App"],
    improvements: [
      "Import routes separately",
      "improve gross salary calculator accuracy",
      "Implement unit and e2e tests",
      "Add more specific triggers for Google Analytics",
      "Run a Facebook ad arketing campaigns",
      "Data analysis on feedback",
      "Hide result on form change (cashflow calc)",
      "Add edit condition functionality",
      "More animation (text & calculate button)",
      "Add more provinces and automate fetching tax brackets",
      "More detailed breakdown salary calc (refer to figma for design)",
      "Salary app walk through. Have a demo button which prefills conditions, shows that you need to open form dialog, and have some reccurent animation (glowing) and tooltip on calculate button to indicate user has to click there",
      "Add normal tax calculator (salary calc)",
      "Save data in cookies for easy reuse",
      "Enable offline use",
      "make navbar app-like at the bottom for mobile",
      "Add money masks for form fields"
    ]
  },
  {
    id: "koverdd",
    name: "Koverdd",
    category: "Consulting",
    url: "https://koverdd.com",
    repoUrl: "https://gitlab.com/Issaf/koverdd",
    imgUrl: "koverdd-bg.jpg",
    problem: "Imagine you own a digital agency. You like to remove any initial friction for interested customers by giving them the option to book a call directly on the plaform no signup, no webinar, or credit card requirement. Koverdd is the digital bootique allowing potential clients to easily see the services offered, case studies of previous clients, and immediately book an appointment to discuss their project.",
    description: "This project is Koverdd digital agency’s bilingual website. It has a simple landing page, a contact page, and a booking manager. It is mostly a project completed to build Vue.js apps from scratch and become more efficient with Vuetify.js. The code is not optimal and will require some refacturing and development to be of value for both new potential clients, and current ones.",
    technologies: ["Javascript", "Vue.js", "Vuetify.js", "Progressive Web App", "Firestore", "EmailJS"],
    improvements: [
      "Rename folders, variables, and functions",
      "Place redundant functionalities into a service (emailer for example)",
      "Add unit tests, and e2e tests",
      "Rewrite some functions to be more pure",
      "Chunk routes to improve performance",
      "SEO for landing page",
      "Better analytics tracking (buttons, actions, pages)",
      "Add edit appointment functionality",
      "Redesign landing page", 
      "Add a page to view all schedules",
      "Add email notification when appointment is booked",
      "Improve email when appointment is booked (add all important details)",
      "Secure calls to firebase (ie. Add rules)",
      "Add server and database to handle user logins and infomation about their project",
      "Add a project tracking section, Allowing clients to track progress, upload files, and leave messages (maybe even handle payments)",
      "Add animations",
      "Update vuetify version and change font family",

    ]
  },
  {
    id: "mtlrents",
    name: "MTLRents",
    category: "Machine Learning",
    url: "https://mtlrents.firebaseapp.com",
    repoUrl: "https://gitlab.com/Issaf/mtlrents",
    imgUrl: "mtlrents-bg.jpg",
    description: "MTLRents is an old project that has been revamped. It does only one thing estimate rent price of an apartment in Montreal given the address, number of bedrooms, and number of bathrooms. It uses a machine learning algorithm. The fontend and backend have been separated. The backend is a flask web app serving one endpoint (ie. The estimator result) while the frontend has been rewritten in Vue.js. The estimator will most likely be inaccurate as the data scrapped is from 2017.",
    problem: "The main problem mtlrents was trying to solve is ‘how can you make sure you paying a fair price in rent if you don’t know the city?’ In fact is not always easy to have that information especially if you are a new immigrant, foreigner, or international student with a completely different currency.",
    technologies: ["Javascript", "Vue.js", "Vuetify.js", "Chart.js", "Python 3", "Flask", "ScikitLearn", "Pandas", "Google App Engine"],
    improvements: []
  },
  {
    id: "mvmtribe",
    name: "mvmtribe",
    category: "Fitness",
    url: null,
    repoUrl: "https://gitlab.com/Issaf/mvmtribe",
    imgUrl: "mvmtribe-bg.jpg",
    description: "Initially a startup, mvmtribe (pronounced movement tribe) is a platform that helps people in the fitness journey. As a customer, you will have access to 3 sections: educating and practicle blog articles, a list of movment specialist near you, and a store to get all the tools you need to stay healthy and strong. As a professional (specialist), you can sign up and have access to very targeted leads. Finally, the admin portal allows to update new content, edit users, add products and change prices.",
    problem: "The fitness industry is heavily focused on performance and aesthetics which is not always healthy. Mvmtribe was built to bridge the gap, and become the reference for people who want to focus on the fundamentals (ie. move often and well, keep a holistic approach, care more about being and staying healthy).",
    technologies: ["Javascript", "Angular 6", "Node.js", "Express.js", "Passport.js", "Nginx", "PostgreSQL", "Mapbox", "AWS S3"],
    improvements: []
  },
  {
    id: "catalog-marketing",
    name: "Catalog Marketing",
    category: "Data Analytics",
    url: null,
    repoUrl: "https://gitlab.com/Issaf/catalog_marketing",
    imgUrl: "catalogmarketing-bg.jpg",
    description: "Analysis of a catalog marketing strategy and predicting the expected profits. Pandas, Sklearn, Matlibplot, and Seaborn were the main libraries used",
    problem: "The goal was to decide if the strategy is profitable enough for the catalogs to be sent out.",
    technologies: ["Python 3", "Pandas", "Matplotlib", "Jupyter Notebook"],
    improvements: []
  }
]

export {
  getProject,
  getProjects
}

function getProject(id) {
  let prj = projects.find(x => x.id === id)
  return prj
}

function getProjects(fields=[]) {
  if (!fields.length) return projects
  else {
    return projects.map(x => {
      let obj = {}
      fields.forEach(key => { obj[key] = x[key] })
      return obj
    })
  }
}