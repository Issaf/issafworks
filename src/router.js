import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/projects/:id',
      name: 'Project',
      component: () => import(/* webpackChunkName: "project" */ './views/Project.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  window.scrollTo(0,0)
  next()
})

export default router